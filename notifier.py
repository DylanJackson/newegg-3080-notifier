from bs4 import BeautifulSoup
import requests
import webbrowser
import time
import random
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def SendEmail(link):
    sender = ""
    passw = ""
    receiver = ""
    smtp = ""
    port = 587
    server = smtplib.SMTP(smtp,port)
    server.starttls()
    server.login(sender,passw)
    
    # Now we use the MIME module to structure our message.
    msg = MIMEMultipart()
    msg['From'] = sender
    msg['To'] = receiver

    # Make sure you also add new lines to your body
    body = link
    # and then attach that body furthermore you can also send html content.
    msg.attach(MIMEText(body, 'html'))
    
    sms = msg.as_string()
    
    # Create the body of the message (a plain-text and an HTML version).
    text = "3080 Found!"
    html = """\
    <html>
    <head></head>
    <body>
        <p>Hi!<br>
            <a href=""" + link + """>3080 Found</a>
        </p>
    </body>
    </html>
    """

    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText(text, 'plain')
    part2 = MIMEText(html, 'html')

    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    msg.attach(part2)
    
    server.sendmail(sender,receiver,sms)
    server.quit()

def CheckNewEgg():
    print("Checking NewEgg...")

    URL = "https://www.newegg.com/p/pl?d=rtx+3080+gpu"
    r = requests.get(URL)
    soup = BeautifulSoup(r.content,'html5lib')
    table = soup.findAll('div',attrs = {'class':'item-container'})
    
    if(r.ok):
        for row in table:
            if(row.button != None):
                href = row.a['href']
                if(not('3090' in href) and (not('Combo' in href))):
                    promoText = row.find('p', class_ = 'item-promo')
                    if(not('OUT OF STOCK' in promoText) or promoText == None):
                        print("Found one at: " + href)
                        webbrowser.open_new(href)
                        SendEmail(href)
                        return False
        print("No stock found at NewEgg")
        return True
    else:
        print("Access Denied to NewEgg")
        return False

def CheckEvga():
    print("Checking EVGA...")
    
    URL = "https://www.evga.com/products/ProductList.aspx?type=0&family=GeForce+30+Series+Family&chipset=RTX+3080"

    r = requests.get(URL)
    soup = BeautifulSoup(r.content,'html5lib')
    
    table = soup.findAll('div',attrs = {'class':'list-item'})
    if(r.ok):
        #print(table[0])
        for row in table:
            buttonDiv = row.find('input', attrs = {'class':'btnBigAddCart'})
            if(buttonDiv != None):
                div = row.find('div', attrs = {'class':'pl-list-pname'})
                hrefDiv = div.find('a')
                
                for att in hrefDiv.attrs.keys():
                    if(att == 'href'):
                        href = "https://www.evga.com" + hrefDiv['href']                        
                        webbrowser.open_new(href)
                        SendEmail(href)
                        return False
        print("No stock found")
        return True
    else:
        print("Access denied")
        return False

def CheckBH():
    print("Checking BH...")
    
    URL = "https://www.bhphotovideo.com/c/products/Graphic-Cards/ci/6567/N/3668461602?filters=fct_nvidia-geforce-series_5011%3Ageforce-rtx-3080"
    r = requests.get(URL)
    soup = BeautifulSoup(r.content,'html5lib')
    
    table = soup.findAll('div',attrs = {'data-selenium':'miniProductPageProduct'})
    if(r.ok):
        for row in table:      
            buttonDiv = row.find('div', attrs = {'data-selenium':'miniProductPageQuantityContainer'})
            button = buttonDiv.find('button')
            if('Add to Cart' in button.text):
                hrefDiv = row.find('a', attrs = {'data-selenium':'miniProductPageProductNameLink'})
                href = hrefDiv['href']
                
                webbrowser.open_new(href)
                SendEmail(href)
                return False
        print("No stock found at BH")
        return True
    else:
        return False
        print("Access Denied to BH")

random.seed(235612)
bContinue = True
while(bContinue):
    bContinue = (CheckBH() and CheckNewEgg() and CheckEvga())
    sleepTime = random.randrange(3,10)
    print("sleeping for " + str(sleepTime) + " seconds")
    time.sleep(sleepTime)
print("Something Found we are done here!")



