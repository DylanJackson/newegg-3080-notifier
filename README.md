Fun little project.

Coworker was saying bots auto bought the entire stock of NVidia RTX 3080s the moment they went on sale and I decided to give it a go.

This program does not use any shopping related API's and cannot buy stuff for you.

It's purpose is to simply check NewEgg, Evga, and B&H.

After checking it will sleep for 4-10 seconds.  The number of seconds is obtained from random.

If it is found, a new instance of your web browser will open to the in stock item where you can add it to your cart and purchase it.
It will also send a text message with a link should you desire.

To use the email and text messaging function fill out the sender, pssw, receiver, and smtp variables in the SendMessage function.
